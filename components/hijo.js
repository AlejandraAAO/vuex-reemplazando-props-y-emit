Vue.component('hijo',{
    template:
    `
    <div>
        <button @click="aumentar"> + </button>
        <button @click="disminuir(2)"> - </button>
        <button @click="getNames"> Obtener nombres </button>
        <h1>numero {{numero}}</h1>
        <ul v-for="item of names">
            <li>{{item.name}}</li>
        </ul>
    </div>
    `,
    computed:{
        //asi utilizamos numero dentro del store
        //llamamos y mapeamos todo el state[ ]
        ...Vuex.mapState([ 
            'numero',
            'names'
        ])
    },
    //mutacioin modifica el numero
    methods:{
        ...Vuex.mapMutations(['aumentar', 'disminuir']),
        //para mapear acciones
        ...Vuex.mapActions(['getNames'])

    }

})