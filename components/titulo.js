Vue.component('titulo',{
    template:
    `
    <div>
        <!--se llama al store con $-->
        <h1>numero {{numero}}</h1>
        <hijo></hijo>
    </div>
    `,
    computed:{
        //asi utilizamos numero dentro del store
        //llamamos y mapeamos todo el state
        ...Vuex.mapState([ 
            'numero'
        ])
    }
})